from zope import interface
from zope.interface import Attribute

class IMessage(interface.Interface):
    content = Attribute("""The text message to log""")

class ISocketMessage(interface.Interface):
    content = Attribute("""Content to send""")

@interface.implementer(IMessage)
class Debug(object):

    def __init__(self, message):
        self.content = message
