from typing import Optional
from zope import interface
from zope.interface import Attribute

class IDesktop(interface.Interface):

    queue = Attribute("""The queue to send the message""")
    mapping = Attribute("""Correspondance between application and layer""")

    def getForegroundWindowTitle(sel) -> Optional[str]:
        """ Return the name of the selected window
        """

    def start(self) -> None:
        """ Start listening the events
        """
