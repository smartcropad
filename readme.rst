.. role:: json(code)
   :language: json

=======================
Smart Macropad with KMK
=======================

.. contents::
   :depth: 2

-------------
The companion
-------------

The companion is running in the PC connected with the macropad. The application
will control the keyboard and changes the keys depending of the application used.

Requirements
============

.. code:: bash

    python3 -m pip install pystray pyserial zope.component

for debian you may need to install :

.. code:: bash

    sudo apt install python3-pil.imagetk python3-serial python3-zope.component python3-pystray

Configuration
=============

The configuration lies in an ini file:

Serial connection
-----------------

.. code:: ini

    [connexion.serial]
    port = \\.\COM12


Initialize a connection directly with the macropad

Socket connection
-----------------

.. code:: ini

    [connexion.socket]
    port = 9999
    host = localhost

Use a proxy to connect with the keyboard (as client)

Socket server
-------------

.. code:: ini

    [socket.serve]
    port = 9999
    host = localhost
    name = The name of the application to associate with

Use a proxy to connect with the keyboard (as server)

The mapping
-----------

.. code:: ini

    [mapping]
    Mozilla Firefox = firefox.json
    Teams = teams.json
    irssi = irssi.json
    …

Mapping list
============

When a new window is selected, the application will look in the table for a
match, and send the corresponding layer to the endpoint.

.. note::

    When using XLib, the application will match the name with both the CLASS or
    the NAME of the selected window

If the application receive a notification for a new layer, it will also
register it, and store it for the session. This allow to make the application
"learn" about the layer you want to use.

Serial connection
=================

Sending message
---------------

The application send a json string to the endpoint (network or serial connection):

.. code:: json

    {"layer_name": "keymap"}

the keymap can be:

:a string:

    The key named will be sent: :json:`"A"` (you have to follow the named
    declared in the device)

:a list:

    All the keys will be chained in a single stroke: :json:`["^", "A"]`

:a dictionnary:

    Used to create custom sequences: :json:`{"seq": ["/", "W", "C", "ENTER"]}`

:null:

    The key will do nothing.

.. code:: json

    { "Example": [
        {"seq": ["A", "B", "C"]}, ["^", "R"], ["^", "T"], ["^", "W"],
        null, null, null, null
    ]}

.. Reading message
.. ---------------
..
.. The endpoint can also send a message to the application. For now, the message
.. is a raw string with the name of the layer.
..
.. When the application receive such message, it will look for the active window
.. in the desktop, and register the application with this layer. If this
.. application is selected again, the application will ask the endpoint to switch
.. to the same layer again.

Network connection
==================

You can relay the events to another one instance using the network. I'm using
this when I'm connected over VNC in order to use the keyboard as if it was
plugged directly in the host.


----------
The client
----------

There is a command-line client, which allow to send a layer directly to the device.

The client can work in interractive mode (usefull for testing), or in a
one-shot action, sending the json file given with the parameter :literal:`--layer`

----------
The device
----------

CircuitPython provide a native library in order `to read or store json`_, and
firmware build upon it (like KMK) are easer to use with.

.. _`to read or store json`: https://docs.circuitpython.org/en/latest/docs/library/json.html

