#!/bin/python3

import sys
from queue import Queue
import json

import argparse
from os import path

from zope import component
from interfaces import endpoint
import configparser

script_path = path.dirname(path.realpath(__file__))
config_file = path.join(script_path, "config.ini")
parser = argparse.ArgumentParser(
                    prog='client',
                    description='Command line client to the keyboard',
                    epilog='')
parser.add_argument('configuration', nargs='?', default=config_file)
parser.add_argument('--layer')
args = parser.parse_args()


config = configparser.ConfigParser(delimiters="=")
config.read(args.configuration)

if config.has_section("connection.socket"):
    print("Socket connexion")
    from socket_conn import SocketConnection
    conn = SocketConnection(config["connection.socket"])
elif config.has_section("connection.serial"):
    print("Serial connecion")
    from serial_conn import SerialConnection
    conn = SerialConnection(config["connection.serial"])

# Connect to the endpoint right now
component.provideAdapter(endpoint.EndPoint)
s = component.queryAdapter(conn, endpoint.IEndpoint)
s.connect()

if args.layer is not None:
    print(args.layer)
    s.queue = Queue()
    with open(args.layer, "r") as json_file:
        json_data = json_file.read()
        j = json.loads(json_data)
        s.send(j)
    sys.exit(0)

from threading import Thread
import time

class Conn(Thread):

    def __init__(self, queue):
        Thread.__init__(self)
        self.queue = queue
        self.in_queue = Queue()
        s.queue = self.in_queue

    def run(self):

        while True:
            while not self.queue.empty():
                text = self.queue.get(False)
                if text == -1:
                    sys.exit(0)
                elif text == "":
                    continue
                j = json.loads(text)
                s.send(j)

            if not s.isConnected():
                time.sleep(2)
                s.connect()
                continue


            s.fetch()

            while not self.in_queue.empty():
                msg = self.in_queue.get(False)
                print(msg)
                # Print the promot again
                print("\n> ", end=u"", flush=True)

            time.sleep(0.2)

queue = Queue()
c = Conn(queue)
c.start()
print("Press ^D to quit")
while True:
    try:
        text = input("> ")
        queue.put(text)
        if text == "":
            continue
    except:
        queue.put(-1)
        break
